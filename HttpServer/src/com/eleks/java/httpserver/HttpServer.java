package com.eleks.java.httpserver;

import com.eleks.java.httpserver.services.HttpRequestImpl;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HttpServer {
    public static void main(String args[]) throws Throwable {
        ServerSocket serverSocket = new ServerSocket(8080);
        while (true) {
            Socket socket = serverSocket.accept();
            System.out.println("Socket accepted.");

            ExecutorService executorService = Executors.newSingleThreadExecutor();
            executorService.execute(new HttpRequestImpl(socket));
        }
    }
}
