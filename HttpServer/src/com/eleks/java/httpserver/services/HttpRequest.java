package com.eleks.java.httpserver.services;

public interface HttpRequest extends Runnable {

    @Override
    void run();

    void readHeaders() throws  Throwable;

    void writeResponse(String string) throws Throwable;
}
