package com.eleks.java.httpserver.services;

import com.eleks.java.httpserver.model.User;
import com.eleks.java.httpserver.model.UserDAO;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class HttpRequestImpl implements HttpRequest {

    private Socket socket;
    private InputStream inputStream;
    private OutputStream outputStream;

    private String operation;

    public HttpRequestImpl(Socket socket) throws Exception {
        this.socket = socket;
        this.inputStream = socket.getInputStream();
        this.outputStream = socket.getOutputStream();
    }

    @Override
    public void run() {
        try {
            readHeaders();
            handleOperation();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void writeResponse(String string) throws Throwable {
        String response = "HTTP/1.1 200 OK\r\n" +
                "Server: localhost/2017-09-27\r\n"+
                "Content-Type: text/html\r\n"+
                "Connection: close\r\n\r\n";
        String result = response+string;
        outputStream.write(result.getBytes());
        outputStream.flush();
    }

    public void readHeaders() throws Throwable {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        while (true) {
            String s = bufferedReader.readLine();
            if (s.contains("GET")||s.contains("POST")||s.contains("PUT")||s.contains("DELETE"))
                    setOperation(s);
             else if (s == null || s.trim().length() == 0) {
                break;
            }
        }
    }

    private void setOperation(String operation){
        this.operation = operation;
    }

    private void handleOperation() throws Throwable{
        if(this.operation.contains("GET")&& this.operation.contains("id"))
        {
            writeResponse("<h1>"+UserDAO.getUser(getId()).toString()+"</h1>");
        }
        else if(this.operation.contains("GET")){
            writeResponse(refactorUsers(UserDAO.getAllUsers()));
        }

        if(this.operation.contains("POST")){
            UserDAO.addUser(getUser());
            writeResponse("<h1>User Added.</h1>");
        }

        if(this.operation.contains("PUT")){
            UserDAO.updateUser(getUser());
            writeResponse("<h1>User Updated.</h1>");
        }

        if(this.operation.contains("DELETE")){
            UserDAO.deleteUser(getId());
            writeResponse("<h1>User Deleted</h1>");
        }
    }

    //HARDCODED!
    private String getId(){
        return this.operation.substring(this.operation.indexOf("d")+2,this.operation.indexOf("H")-1);
    }

    //HARDCODED!
    private User getUser(){
        User user = new User();
        //get Id from requestString
        //Example: http://localhost:8080/?id=01&name=Andrii&position=Devekoper; returns 01 
        user.setId(operation.substring(operation.indexOf("d")+2,operation.indexOf("&")));
        String substringName = this.operation.substring(this.operation.indexOf("&")+1);

        //getName from requestString
        user.setName(substringName.substring(substringName.indexOf("e")+2,substringName.indexOf("&")));
        String substringPosition = substringName.substring(substringName.indexOf("&")+1);

        //get Position from requestString
        user.setPosition(substringPosition.substring(substringPosition.indexOf('n')+2,substringPosition.indexOf(';')));
        return user;
    }

    private String refactorUsers(List<User>users){
        StringBuilder result = new StringBuilder();
        for (User user:users
             ) {
            result.append("<h1>").append(user.toString()).append("</h1>");
        }
        return result.toString();
    }
}
