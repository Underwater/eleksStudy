package com.eleks.java.httpserver.model;

import java.util.*;

public class UserDAO {

    private static final Map<String, User> userMap = new HashMap<String, User>();

    static {
        initUsers();
    }

    private static void initUsers(){
        User user1 = new User("01","Andrii","Developer;");
        User user2 = new User("02","Taras", "PM;");
        User user3 = new User("03","Vasilii","Q&A;");

        userMap.put(user1.getId(),user1);
        userMap.put(user2.getId(),user2);
        userMap.put(user3.getId(),user3);
    }

    public static User getUser(String id){
        return userMap.get(id);
    }

    public static User addUser(User user){
        userMap.put(user.getId(),user);
        return user;
    }

    public static User updateUser(User user){
        userMap.put(user.getId(),user);
        return user;
    }

    public static void deleteUser(String id){
        userMap.remove(id);
    }

    public static List<User> getAllUsers(){
        Collection<User> collection = userMap.values();
        List<User>list = new ArrayList<User>();
        list.addAll(collection);
        return list;
    }

}
