package com.eleks.java.httpserver.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class User {
    private final StringProperty id;
    private final StringProperty name;
    private final StringProperty position;

    public User(){
        this(null,null,null);
    }

    public User(String id, String name, String position){
        this.id = new SimpleStringProperty(id);
        this.name = new SimpleStringProperty(name);
        this.position = new SimpleStringProperty(position);
    }

    public String getId() {
        return id.get();
    }

    public void setId(String id){
        this.id.set(id);
    }

    public StringProperty idProperty() {
        return id;
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name){
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

    public String getPosition() {
        return position.get();
    }

    public void setPosition(String position){
        this.position.set(position);
    }

    public StringProperty positionProperty() {
        return position;
    }

    @Override
    public String toString() {
        return id.get()+". "+name.get()+" : "+position.get()+"\n";
    }
}
